from schema import Schema, And, Use, SchemaError
import os
import logging



def check(conf_schema, conf):
    try:
        conf_schema.validate(conf)
        return True
    except SchemaError as err:
        print(str(err))
        return False


configuration = {
    'ORIGIN_HOST': os.getenv('ORIGIN_HOST'),
    'ORIGIN_PORT': int(os.getenv('ORIGIN_PORT', "9200")),
    'ORIGIN_USERNAME': os.getenv('ORIGIN_USERNAME'),
    'ORIGIN_PASSWORD': os.getenv('ORIGIN_PASSWORD'),
    'DESTINATION_HOST': os.getenv('DESTINATION_HOST', os.getenv('ORIGIN_HOST')),
    'DESTINATION_PORT': int(os.getenv('DESTINATION_PORT', "9200")),
    'DESTINATION_USERNAME': os.getenv('DESTINATION_USERNAME', os.getenv('ORIGIN_USERNAME')),
    'DESTINATION_PASSWORD': os.getenv('DESTINATION_PASSWORD', os.getenv('ORIGIN_PASSWORD')),
    'DESTINATION_INDEX': "{}".format(os.getenv('DESTINATION_INDEX')),
    'UPDATE_FREQUENCY': int(os.getenv('UPDATE_FREQUENCY', 30)),
    'VERIFY_SSL': True if os.getenv('VERIFY_SSL', "true").lower() == "true" else False,
    'TEMPLATE_REPLICAS': int(os.getenv('TEMPLATE_REPLICAS', "1")),
    'TEMPLATE_SHARDS': int(os.getenv('TEMPLATE_SHARDS', "1")),
    'POLICY_ROLLOVER_SIZE': int(os.getenv('POLICY_ROLLOVER_SIZE', 10)),
    'POLICY_RETENTION': int(os.getenv('POLICY_RETENTION', 10)),
    'LOG_LEVEL': os.getenv('LOG_LEVEL', "WARNING")
}

# Define Configuration Schema
conf_schema = Schema({
    'ORIGIN_HOST': And(Use(str)),
    'ORIGIN_PORT': And(Use(int)),
    'ORIGIN_USERNAME': And(Use(str)),
    'ORIGIN_PASSWORD': And(Use(str)),
    'DESTINATION_HOST': And(Use(str)),
    'DESTINATION_PORT': And(Use(int)),
    'DESTINATION_USERNAME': And(Use(str)),
    'DESTINATION_PASSWORD': And(Use(str)),
    'DESTINATION_INDEX': And(Use(str)),
    'UPDATE_FREQUENCY': And(Use(int)),
    'VERIFY_SSL': And(Use(bool)),
    'TEMPLATE_REPLICAS': And(Use(int)),
    'TEMPLATE_SHARDS': And(Use(int)),
    'POLICY_ROLLOVER_SIZE': And(Use(int)),
    'POLICY_RETENTION': And(Use(int)),
    'LOG_LEVEL': And(Use(str), lambda n: n in ['CRITICAL','FATAL','ERROR','WARNING','WARN','INFO','DEBUG','NOTSET'])
})


def startup():
    if check(conf_schema, configuration):
        print(
            "------------ Opensearch Cluster Monitor------------\n",
            "Origin Cluster Endpoint: {}:{}\n".format(configuration['ORIGIN_HOST'],
                                                      configuration['ORIGIN_PORT']),
            "Destination Index: {}:{}/{}\n".format(configuration['DESTINATION_HOST'],
                                                   configuration['DESTINATION_PORT'],
                                                   configuration['DESTINATION_INDEX']),
            "Frequency: {}\n".format(configuration['UPDATE_FREQUENCY']),
            "SSL Verification: {}\n".format(configuration['VERIFY_SSL']),
            "------------ Opensearch Cluster Monitor------------\n",
        )
        # If variables are set, add the path to the requests variable
        #if configuration['CUSTOM_SSL'] is not None and configuration['VERIFY_SSL']:
        #    os.environ['REQUESTS_CA_BUNDLE'] = configuration['CUSTOM_SSL']
        if not configuration['VERIFY_SSL']:
            import urllib3
            urllib3.disable_warnings()
    else:
        print("Invalid configuration, quitting")
        quit()
