from app.configurations import configuration
from requests import Session, Response
from requests.adapters import Retry, HTTPAdapter
import traceback
from time import sleep
def setup():
    from main import logger
    try:
        logger.info("Setting up index template")
        index_template()
        logger.info("Setting up index policy")
        index_policy()
        logger.info("Setting up alias")
        alias()
    except Exception:
        logger.error(traceback.format_exc())
        logger.error("Setup failed, proceeding with monitor.")

def index_policy():
    from main import logger
    policy = {
                "policy": {
                    "policy_id": "cluster-monitor",
                    "description": "Created by cluster-monitor",
                    "default_state": "hot",
                    "states": [
                        {
                            "name": "hot",
                            "actions": [
                                {
                                    "retry": {
                                        "count": 3,
                                        "backoff": "exponential",
                                        "delay": "1m"
                                    },
                                    "rollover": {
                                        "min_primary_shard_size": "{}gb".format(configuration['POLICY_ROLLOVER_SIZE']),
                                        "copy_alias": False
                                    }
                                }
                            ],
                            "transitions": [
                                {
                                    "state_name": "delete",
                                    "conditions": {
                                        "min_index_age": "{}d".format(configuration['POLICY_RETENTION'])
                                    }
                                }
                            ]
                        },
                        {
                            "name": "delete",
                            "actions": [
                                {
                                    "retry": {
                                        "count": 3,
                                        "backoff": "exponential",
                                        "delay": "1m"
                                    },
                                    "delete": {}
                                }
                            ],
                            "transitions": []
                        }
                    ],
                    "ism_template": [
                        {
                            "index_patterns": [
                                "{}-*".format(configuration['DESTINATION_INDEX'])
                            ],
                            "priority": 0
                        }
                    ]
                }
            }
    logger.debug(policy)
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    response :Response = s.put("https://{}:{}/_plugins/_ism/policies/cluster-monitor".format(configuration['DESTINATION_HOST'],
                                                                                     configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL'],
                     json=policy)
    if response.status_code < 299:
        logger.info("Policy created successfully.")
    elif response.status_code == 409:
        logger.info("Policy already exists")
    else:
        logger.error("Could not set index policy. Status: {}. Error: {}".format(response.status_code, response.text))


def index_template():
    from main import logger
    template = {
        'index_patterns': ['{}*'.format(configuration['DESTINATION_INDEX'])],
        'template': {
            'settings': {
                'index.plugins.index_state_management.rollover_alias': configuration['DESTINATION_INDEX'],
                'index.number_of_shards': configuration['TEMPLATE_SHARDS'],
                'index.number_of_replicas': configuration['TEMPLATE_REPLICAS']
            }
        }
    }
    logger.debug(template)
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    response :Response = s.put("https://{}:{}/_index_template/cluster-monitor-template".format(configuration['DESTINATION_HOST'],
                                                                       configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL'],
                     json=template)
    if response.status_code > 299:
        raise Exception("Could not set index template. Status: {}. Error: {}".format(response.status_code, response.text))


def alias():
    from main import logger
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    alias_data = {
        'aliases': {
            '{}'.format(configuration['DESTINATION_INDEX']): {
                'is_write_index': True
            }
        }
    }
    logger.debug(alias_data)
    response  :Response = s.get("https://{}:{}/_alias/{}".format(configuration['DESTINATION_HOST'],
                                       configuration['DESTINATION_PORT'],
                                       configuration['DESTINATION_INDEX']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL'])
    if response.status_code == 200:
        logger.info("Alias or index already exists")
    elif response.status_code == 404:
        logger.debug("Creating initial index with alias")
        response  :Response = s.put("https://{}:{}/{}-000001".format(configuration['DESTINATION_HOST'],
                                                  configuration['DESTINATION_PORT'],
                                                  configuration['DESTINATION_INDEX']),
                         auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                         verify=configuration['VERIFY_SSL'],
                         json=alias_data)
        if response.status_code > 299:
            raise Exception("Could not create alias. Status: {}. Error: {}".format(response.status_code, response.text))
    else:
        raise Exception("Could not create alias. Status: {}. Error: {}".format(response.status_code, response.text))
