from requests import Session
from requests.adapters import Retry, HTTPAdapter
from main import logger
from datetime import datetime

def upload(metricset, doc):
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    payload = {
        '@timestamp': "{}".format(datetime.now().isoformat()),
        'metricset': metricset,
        '{}'.format(metricset): doc
    }
    logger.debug(payload)
    logger.info("Logging {}".format(metricset))
    response = s.post("https://{}:{}/{}/_doc".format(configuration['DESTINATION_HOST'],
                                                     configuration['DESTINATION_PORT'],
                                                     configuration['DESTINATION_INDEX']),
                      auth=(configuration['DESTINATION_USERNAME'], configuration['DESTINATION_PASSWORD']),
                      verify=configuration['VERIFY_SSL'],
                      json=payload)
    if response.status_code > 299:
        logger.error("Error writing to destination cluster. Error: {}".format(response.text))

