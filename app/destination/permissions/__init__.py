from requests import Session
from requests.adapters import Retry, HTTPAdapter
from main import logger
from datetime import datetime


def test():
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    # Test Destination Index
    logger.info("Testing permissions on destination cluster")
    doc = {'@timestamp': "".format(datetime.now().isoformat())}
    response = s.post("{}:{}/{}/_doc".format(configuration['DESTINATION_HOST'],
                                             configuration['DESTINATION_PORT'],
                                             configuration['DESTINATION_INDEX']),
                      auth=(configuration['DESTINATION_USERNAME'], configuration['DESTINATION_PASSWORD']),
                      verify=configuration['VERIFY_SSL'],
                      json=doc)
    if response.status_code > 299:
        logger.error("Origin health permissions are not properly set. Error: {}".format(response.text))
