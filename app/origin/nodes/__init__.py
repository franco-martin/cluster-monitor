from requests import Session
from requests.adapters import Retry, HTTPAdapter
import app.destination.write as write


def get():
    from main import logger
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    response = s.get("https://{}:{}/_nodes".format(configuration['DESTINATION_HOST'],
                                                    configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL']
                     )
    if response.status_code > 299:
        logger.error("Could not get nodes. Error: {}".format(response.text))
    else:
        for node in response.json()['nodes'].keys():
            data = response.json()['nodes'][node]
            del data['settings']
            del data['os']
            del data['jvm']['memory_pools']
            del data['thread_pool']
            del data['transport']
            del data['http']
            del data['plugins']
            del data['modules']
            del data['ingest']
            del data['aggregations']
            write.upload('node', data)
