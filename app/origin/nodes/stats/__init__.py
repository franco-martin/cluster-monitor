from requests import Session
from requests.adapters import Retry, HTTPAdapter
import app.destination.write as write


def get():
    from main import logger
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    response = s.get("https://{}:{}/_nodes/stats".format(configuration['DESTINATION_HOST'],
                                                    configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL']
                     )
    if response.status_code > 299:
        logger.error("Could not get node stats. Error: {}".format(response.text))
    else:
        for node in response.json()['nodes'].keys():
            data = response.json()['nodes'][node]
            del data['os']
            del data['process']
            del data['jvm']['mem']['pools']
            del data['jvm']['threads']
            del data['jvm']['buffer_pools']
            del data['jvm']['classes']
            del data['thread_pool']
            del data['transport']
            del data['http']
            del data['breakers']
            del data['script']
            del data['discovery']
            del data['ingest']
            del data['adaptive_selection']
            del data['script_cache']
            write.upload('node_stats', data)
