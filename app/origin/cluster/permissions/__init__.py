from requests import Session
from requests.adapters import Retry, HTTPAdapter
from main import logger
from datetime import datetime


def test():
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    logger.info("Testing permissions on origin cluster")
    # Test health endpoint
    response = s.get("https://{}:{}/_cluster/health".format(configuration['DESTINATION_HOST'],
                                                            configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL'])
    if response.status_code > 299:
        logger.error("Origin health permissions are not properly set. Error: {}".format(response.text))
    # Test stats endpoint
    response = s.get("https://{}:{}/_cluster/stats".format(configuration['DESTINATION_HOST'],
                                                           configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL'])
    if response.status_code > 299:
        logger.error("Origin stats permissions are not properly set. Error: {}".format(response.text))

