from requests import Session
from requests.adapters import Retry, HTTPAdapter
import app.destination.write as write


def get():
    from main import logger
    from app.configurations import configuration
    s = Session()
    retries = Retry(total=5, backoff_factor=1)
    s.mount('https://', HTTPAdapter(max_retries=retries))
    response = s.get("https://{}:{}/_cluster/health".format(configuration['DESTINATION_HOST'],
                                                    configuration['DESTINATION_PORT']),
                     auth=(configuration['ORIGIN_USERNAME'], configuration['ORIGIN_PASSWORD']),
                     verify=configuration['VERIFY_SSL']
                     )
    if response.status_code > 299:
        logger.error("Could not get cluster health. Error: {}".format(response.text))
    else:
        write.upload('cluster_health', response.json())
