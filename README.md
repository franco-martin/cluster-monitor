# Opendistro Cluster Monitor
This project is an monitor for an elasticsearch OSS compatible cluster. For now, metricbeat OSS doesnt support cluster monitoring, so I had to write this.

## Features
The image monitors the following:
- Cluster Health (https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-health.html)
- Cluster Stats (https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-stats.html)
## Configuration
  - ORIGIN_HOST
  - ORIGIN_PORT
  - ORIGIN_USERNAME
  - ORIGIN_PASSWORD
  - DESTINATION_HOST
  - DESTINATION_PORT
  - DESTINATION_USERNAME
  - DESTINATION_PASSWORD
  - DESTINATION_INDEX
  - UPDATE_FREQUENCY
  - VERIFY_SSL
  - POLICY_REPLICAS
  - POLICY_ROLLOVER_DAYS
  - POLICY_RETENTION
  - LOG_LEVEL


## Required permissions
Create a role with the following API call and then map a user to it. Use the users credentials for ORIGIN_USERNAME and ORIGIN_PASSWORD
```
PUT _plugins/_security/api/roles/cluster-monitor
{
  "cluster_permissions": [
      "cluster:monitor/health",
      "cluster:monitor/stats",
      "cluster:monitor/nodes/stats",
      "cluster:monitor/nodes/info",
      "cluster:admin/opendistro/ism/policy/write",
      "indices:admin/index_template/put"
  ],
  "index_permissions": [{
    "index_patterns": [
      "cluster-monitor*"
    ],
    "dls": "",
    "fls": [],
    "masked_fields": [],
    "allowed_actions": [
      "index",
          "create_index"
    ]
  },{
        "index_patterns": [
          "*"
        ],
        "dls": "",
        "fls": [],
        "masked_fields": [],
        "allowed_actions": [
          "indices:admin/aliases/get",
          "indices:admin/aliases",
          "indices:admin/create"
        ]
      }],
  "tenant_permissions": [{
    "tenant_patterns": [
      "human_resources"
    ],
    "allowed_actions": [
      "kibana_all_read"
    ]
  }]
}
```
