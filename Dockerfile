FROM registry.hub.docker.com/library/python:3.9-alpine as build
WORKDIR /monitor
RUN pip3 install virtualenv && \
    python3 -m venv .
COPY requirements.txt /monitor/
RUN bin/pip3 install -r requirements.txt
COPY main.py /monitor/
COPY app /monitor/app


FROM registry.hub.docker.com/library/python:3.9-alpine
WORKDIR /monitor
COPY --from=build /monitor /monitor
ENV PYTHONUNBUFFERED=1
USER 1001
CMD ["./bin/python3","main.py"]
