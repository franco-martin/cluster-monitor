import os
import logging
import app.origin.cluster.health as health
import app.origin.cluster.stats as stats
import app.origin.nodes as nodes
import app.origin.nodes.stats as node_stats
import signal
from time import sleep
from app.destination import setup
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s')

logger = logging.getLogger()
logger.setLevel(level=("INFO" if os.getenv("LOG_LEVEL") is None else os.getenv("LOG_LEVEL")))


class GracefulKiller:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self, signum, frame):
    logger.info("Signal received, exiting now.")
    exit(0)

if __name__ == "__main__":
    from app.destination.setup import setup
    import app.configurations as config
    killer = GracefulKiller()
    logger.info("Startup")
    logger.info("Validating configurations")
    config.startup()
    setup()
    logger.info("Starting Monitor")
    while not killer.kill_now:
        health.get()
        stats.get()
        nodes.get()
        node_stats.get()
        sleep(config.configuration['UPDATE_FREQUENCY'])

